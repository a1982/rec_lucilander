package com.example.recpdm.ui.theme.components.comp.bottomnav

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.example.recpdm.R


sealed class BottomNavItem(
    val route: String,
    @StringRes val titleRes: Int,
    @DrawableRes val iconRes: Int
) {
    object Car: BottomNavItem("car", R.string.car, R.drawable.ic_car)
    object Cadastrar: BottomNavItem("cadastrar", R.string.cadastrar, R.drawable.ic_cadastrar)
}