package com.example.recpdm.ui.theme.components.comp.bottomnav

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.example.recpdm.ui.theme.components.comp.screens.CadastrarScreen
import com.example.recpdm.ui.theme.components.comp.screens.CarsScreen


@Composable
fun BottomNavGraph(navHostController: NavHostController) {
    NavHost(
        navController = navHostController,
        startDestination = BottomNavItem.Car.route
    ) {
        composable(BottomNavItem.Car.route) {
            CarsScreen()
        }
        composable(BottomNavItem.Cadastrar.route) {
            CadastrarScreen()
        }
    }
}