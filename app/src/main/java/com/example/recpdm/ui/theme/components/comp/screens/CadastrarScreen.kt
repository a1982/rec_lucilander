package com.example.recpdm.ui.theme.components.comp.screens

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview

@Composable
fun CadastrarScreen(){
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.fillMaxSize()
    ) {
        OutlinedTextField(
            value = "",
            onValueChange = { },
            label = { Text(text = stringResource(id = com.example.recpdm.R.string.descricao))}
        )

        OutlinedTextField(
            value = "",
            onValueChange = { },
            label = { Text(text = stringResource(id = com.example.recpdm.R.string.valorMin))}
        )

        DropdownMenu(expanded = false, onDismissRequest = { /*TODO*/ }) {

        }
        DropdownMenuItem(onClick = { /*TODO*/ }) {

        }

        Button(onClick = { /*TODO*/ }) {
            Text(text = stringResource(id = com.example.recpdm.R.string.addCar))
        }
    }

}


