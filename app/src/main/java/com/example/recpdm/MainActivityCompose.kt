package com.example.recpdm

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.R
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.recpdm.ui.theme.components.comp.screens.CadastrarScreen

class FormActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent { AppContent()
            }
    }

    @Composable
    private fun AppContent() {
        Scaffold(topBar = { TopBar() }) {
            CadastrarScreen()
            buttonAdd()
        }
    }

    @Composable
    private fun TopBar(){
        TopAppBar (
            title = { Text(text = stringResource(id = com.example.recpdm.R.string.app_name)) },
            backgroundColor = colorResource(id = com.example.recpdm.R.color.purple_500),
            contentColor = Color.White
        )
    }

    @Composable
    fun CadastrarScreen(){
        Row() {
            val checkedState = remember{
                mutableStateOf(false)
            }
            Checkbox(checked = checkedState.value, onCheckedChange = {checkedState.value = it}, modifier = Modifier.padding(top = 50.dp))
            
            Image(painter = painterResource(id = com.example.recpdm.R.drawable.relampagomarquinhos),
                contentDescription = "Car Image",
                modifier = Modifier
                    .padding(9.dp)
                    .width(100.dp)
                    .height(110.dp)
                    .fillMaxHeight()
                )

            Column(
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier.fillMaxSize()
            ) {
                OutlinedTextField(
                    value = "",
                    onValueChange = { },
                    label = { Text(text = stringResource(id = com.example.recpdm.R.string.descricao))}
                )

                OutlinedTextField(
                    value = "",
                    onValueChange = { },
                    label = { Text(text = stringResource(id = com.example.recpdm.R.string.valorMin))}
                )

                DropdownMenu(expanded = false, onDismissRequest = { /*TODO*/ }) {

                }
                DropdownMenuItem(onClick = { /*TODO*/ }) {

                }
            }
        }

    }

    @Composable
    fun buttonAdd(){
        FloatingActionButton(onClick = { "Adicionar Novo Carro!" }) {
            Icon(imageVector = Icons.Filled.Add, contentDescription = "Add Button"
            )
        }
    }

    @Preview
    @Composable
    private fun DefaultPreview(){
        AppContent()
    }

}